## ECDSA Linkable Ring Signatures

This repo contains an implementation of `ECDSA('secp256k1')` linkable ring signatures in TypeScript / JavaScript.

The code is based on <https://github.com/noot/ring-go/tree/linkable>.

### Get Started

Install the package:

```sh
npm install lrs-ecdsa
```

### Example

Copy the following on your NodeJS terminal, or run `node example.js`

```javascript
// import { sign, verify, link } from 'lrs-ecdsa'
const { sign, verify, link } = require('lrs-ecdsa')

// The private key we use to sign
const privateKeyInCensus = "358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803"
const myPublicKey = "04883e1450d9c3068eac73f7e9f136521db8ce351fba4a1739a3cbeb6800172621c862d8ef12104586bc5fd0c9ad23c5f23da32ce99c4daac4112369f84412e069"

// The public keys of the ring
const censusPublicKeys = [
	"045655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c068a",
	"046655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c068b",
	"047655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c068c",
	"048655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c068d",

	// our public key:
	myPublicKey,

	"049655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c068e",
	"049655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c0680",
	"049655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c0681",
	"049655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c0682",
	"049655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c0683"
]

// VALID SIGNATURES

const signature1 = sign("hello world", privateKeyInCensus, censusPublicKeys)
const isValid1 = verify(signature1, censusPublicKeys)

const signature2 = sign("hi there", privateKeyInCensus, censusPublicKeys)
const isValid2 = verify(signature2, censusPublicKeys)

const signaturesLinked = link(signature1, signature2)

console.log(isValid1 ? " ✅ Signature 1 is valid" : "❌ Signature 1 is invalid")
console.log(isValid2 ? " ✅ Signature 2 is valid" : "❌ Signature 2 is invalid")
console.log(signaturesLinked ? " ✅ Signatures 1 and 2 are linked" : "❌ Signatures 1 and 2 are not linked")

// INVALID  SIGNATURES

try {
	// A random private key not in the ring
	const extraneousPrivateKey = "3000e44145a006a1add8622786b0007e0b00391e072855a5667eb3c78b9d3000"

	const fakeSignature = sign("hello world", extraneousPrivateKey, censusPublicKeys)
	throw new Error("❌ Should have thrown an error")
}
catch (err) {
	if (err.message != "The given key pair does not match with any public key in the array") {
		console.error(err)
	}
	else {
		console.log(" ✅ The fakePrivate key is not within the set of public keys")
	}
}

// ALTERED SIGNATURES

try {
	// A random private key not in the ring
	const extraneousPrivateKey = "3000e44145a006a1add8622786b0007e0b00391e072855a5667eb3c78b9d3000"
	const publicKeyFromExtraneous = "04" + "e4231b3341721334b190c3b7c993c1197a1a24382f609b1eef3c2fbc6392364189f717d24c7a2b328672d17d59c95338d4cd69be1001bd8f87bed953691512b8"

	// replace one of the keys with an extraneous one, so we can try to sign with another account
	const pubKeysAltered = censusPublicKeys.map((pubKey, i) => {
		if (i == 0) return publicKeyFromExtraneous
		return pubKey
	})
	const extraneousSignature1 = sign("hello world", extraneousPrivateKey, pubKeysAltered)

	const isValid3 = verify(extraneousSignature1, censusPublicKeys)

	const extraneousSignature2 = sign("hi there", extraneousPrivateKey, pubKeysAltered)
	const extraneousSignaturesLinked2 = link(extraneousSignature1, extraneousSignature2)

	const differentSignaturesLinked = link(signature1, extraneousSignature2)

	console.log(isValid3 ? "❌ The altered signature is valid" : " ✅ The altered signature is invalid")
	console.log(extraneousSignaturesLinked2 ? " ✅ Altered signatrues 1 and 2 are linked" : "❌ Altered signatrues 1 and 2 are not linked")
	console.log(differentSignaturesLinked ? "❌ Signature 1 and altered signature 2 are linked" : " ✅ Signature 1 and altered signature 2 are not linked")
}
catch (err) {
	console.log(err)
}

```

### Development

```shell
git clone git@github.com:vocdoni/lrs-ecdsa.git
cd lrs-ecdsa
npm install
npm run build
npm run test
```
