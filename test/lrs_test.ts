import { expect } from "chai"
import { sign, verify, link } from "../src/lib/ring"
import { sign as mainSign } from "../src/index"
import { makeKeyring, getKeyImage, hashPoint } from "../src/lib/utils";
import { ec as EC } from 'elliptic'
import { SHA3 as SHA3 } from 'sha3'
import { Buffer } from 'buffer';
import * as test_util from './utils';
import { Wallet } from "ethers";

const pubKeyList = [
    "045655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c068a",
    "046655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c068b",
    "047655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c068c",
    "048655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c068d",
    "049655feed4d214c261e0a6b554395596f1f1476a77d999560e5a8df9b8a1a3515217e88dd05e938efdd71b2cce322bf01da96cd42087b236e8f5043157a9c068e"
]


describe("Given a message, a ring, a keypair to sign and its position we can create a valid LRS", () => {
    it('should not accept a ring with size less than 2', () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        try {
            sign(new Uint8Array(), [keyPair], keyPair, 1);
            throw new Error('it should throw an error but it did not')
        } catch (e) {
            expect(e.message).to.contain("size less than")
        }
    })
    it('should not accept a signer to be placed out of bounds in the ring', () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        try {
            sign(new Uint8Array(), [keyPair, keyPair, keyPair], keyPair, 5);
            throw new Error('it should throw an error but it did not')
        } catch (e) {
            expect(e.message).to.contain("secret index out of")
        }
        try {
            sign(new Uint8Array(), [keyPair, keyPair, keyPair], keyPair, -1);
            throw new Error('it should throw an error but it did not')
        } catch (e) {
            expect(e.message).to.contain("secret index out of")
        }
    })
    it('should generate a valid signature given a message, a key ring of 12, a signer key pair and its position', () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        var size = 12;
        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);

        //const position = Math.floor(Math.random() * size);
        const position = 7
        let ring = test_util.GenNewKeyRingTEST(size, keyPair, position);
        let s = test_util.SignTEST(payload, ring, keyPair, position);
        let res = s.C.toString(10)
        expect(res).to.be.equal("95212137913632929640472938188179165865815633825010901564178800776691000715157")
    })
    it('should generate a valid signature given a message, a key ring of 100, a signer key pair and its position', () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        var size = 100;
        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);
        const position = Math.floor(Math.random() * size);
        let ring = test_util.GenNewKeyRing(size, keyPair, position);
        let s = sign(payload, ring, keyPair, position);
        expect(s.Ring.length).to.be.equal(100)
    })
})

describe('Signed the message, the verification process returns true', () => {
    it('Should verify a sig of 10 elements', () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        var size = 10;
        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);
        const position = Math.floor(Math.random() * size);
        let ring = test_util.GenNewKeyRing(size, keyPair, position);
        let s = sign(payload, ring, keyPair, position);
        let v = test_util.VerifyTEST(s);
        expect(v).to.be.equal(true)
    })
    it('Should verify a sig created with 10 known public keys', () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        var size = 10;
        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);
        const position = Math.floor(Math.random() * size);
        let ring = test_util.GenNewKeyRingTEST(size, keyPair, position);
        let s = test_util.SignTEST(payload, ring, keyPair, position);
        let v = test_util.VerifyTEST(s);
        expect(v).to.be.equal(true)
    })
    it('Should verify a sig of 100 elements', () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        var size = 10;
        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);
        const position = Math.floor(Math.random() * size);
        let ring = test_util.GenNewKeyRing(size, keyPair, position);
        let s = sign(payload, ring, keyPair, position);
        let v = test_util.VerifyTEST(s);
        expect(v).to.be.equal(true)
    })
    it('Should verify a sig of 100 elements with a ring with elements located in diferent positions', () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        var size = 10;
        const hash = new SHA3(256);
        hash.update('vocdoni');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);
        const position = Math.floor(Math.random() * size);
        let ring = test_util.GenNewKeyRing(size, keyPair, position);
        let s = sign(payload, ring, keyPair, position);
        s.Ring.sort();
        let v = test_util.VerifyTEST(s)
        expect(v).to.be.equal(true)
    })
    it('should not verify a signature when input keys and ouput keys are not the same keys', () => {
        // The private key we use to sign
        var ec = new EC('secp256k1');
        let privKey = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        let privKey2 = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3804", 'hex')
        var size = 13;

        let keyList = [
            "045cbb56ac7f719a4950f2bb32144e02d987b67c6ebcda6c69daa7336d60aeea30197c32ebdc0cf7a4f43365c0d118e75b5acf5886118e1aade9330f118c5e4e9c",
            "043f4f74781dd9c87258347d64d0726d958061daff78d5f9a763f27cb2cd0588c8e08171bceb6fcb62ef8585f947ede9b8c5f82ad72a067d8ba827fd8710a1494a",
            "0405e62c631d549b7e162b02567265508c70cc99f30ca3754e94119ca981ac312c33c47c573e208968d9ce3de17f31ce47d718746a8cef66d1904f1e90b66da700",
            "0442e440a0054e88deee5b4039d08dd2407a3abe39f01cd5864edbec91ac8bab527ffe79e635195a7557cfc81c28721f8d621559687435ce11ee9f9478d9f78a10",
            "04d701d47dd31280cdf0ff791cb4d15a8fd9b6c4b77d1dbefb0d77d70186b8bc8f74cf446d1876607c2f2bcdae2ea5169451f97e5c64ed26787eca701e48b05f62",
            "04aa3988a2736e02a6de25e2da96bd8f045ae81312453a7ce098a71fa8b86516c9ab01e685cdfeb389f23ef9e7e9aea96aa97318c71de71c17784e3a0122695a3e",
            //"0423616e3f4009fe3cd63a56e21b898e03c9eaa716974555079541e3593a30c7dee9529c589259de7983fe03c3d75be669fac27974393a783f6cf4c742c8aa6cac", priv2pub
            "04883e1450d9c3068eac73f7e9f136521db8ce351fba4a1739a3cbeb6800172621c862d8ef12104586bc5fd0c9ad23c5f23da32ce99c4daac4112369f84412e069", // privpub
            "04a2f3c17b731688494577d8baa393d2f362cc4d876644c5998db299a419e2f1123889e1d1f322ee1800cbcaf7a54f776a9489baa935798c3f51d5926866984a27",
            "045cbb56ac7f719a4950f2bb32144e02d987b67c6ebcda6c69daa7336d60aeea30197c32ebdc0cf7a4f43365c0d118e75b5acf5886118e1aade9330f118c5e4e9c",
            "0476b27d34747b90f0fb4f8b49f6c7a2184d00d2dab72d15f34894060fab77e84222d4539365dd0e94729da01bae2af29a7847ac6fb34112506fbf1e2731bff220",
            "04f79011bf20e814db1330a6db981de7f4ab8e666d730fbbcd2df11ed8622773ed030141783ebc9075c9816d64c6a85635ea050d7c28b855244b6586e5c8db39ed",
            "0402309ddace73d46c02ebf66cd58bcce761a1a4e7a6e23415fa066be4298eb6c17e2caf238502d06a0aa8bccd940f80b202a4bf9db6e714a892bc8be144c075d1",
            "04115f9c0c5b9bf7221ec0d6791a1f77c02057bf9249e73b6c60618b434294f6cbd9430df2e5b1d25c49912ec1797bc4818c9e0513292a92e429aa22285bedd861"
        ]

        const hash = new SHA3(256);
        hash.update("helloworld");
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);

        const hash2 = new SHA3(256);
        hash2.update("hi there");
        let message_hash2 = hash2.digest('hex');
        let input2 = Buffer.from(message_hash2, 'hex');
        const payload2 = Uint8Array.from(input2);

        let position = 7;
        let ring = test_util.GenNewKeyRingTEST(size, privKey, position);
        const signature = sign(payload, ring, privKey, position)

        let ring2 = test_util.GenNewKeyRingTEST(size, privKey2, position);
        const signature2 = sign(payload, ring2, privKey2, position)

        let isValid = verify(signature, keyList)
        let isValid2 = verify(signature2, keyList)

        expect(isValid).to.be.equal(true)
        expect(isValid2).to.be.equal(false)
    })
})
describe('Given random signatures we can link them if their are linkable', () => {
    it('should link two signatures that are equal', () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        var size = 10;
        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);
        const position = Math.floor(Math.random() * size);
        let ring = test_util.GenNewKeyRingTEST(size, keyPair, position);
        let s = test_util.SignTEST(payload, ring, keyPair, position);
        let s2 = test_util.SignTEST(payload, ring, keyPair, position);
        let res = link(s, s2)
        expect(res).to.be.equal(true)
    })
    it('should link two signatures that are created using the same privkey', () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        var size = 10;
        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);
        const position = Math.floor(Math.random() * size);
        let ring = test_util.GenNewKeyRing(size, keyPair, position);
        let s = sign(payload, ring, keyPair, position);
        let s2 = sign(payload, ring, keyPair, position);
        let res = link(s, s2)
        expect(res).to.be.equal(true)
    })
    it('should not link two signatures that are created using diferent private keys', () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        let keyPair2 = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3804", 'hex');
        var size = 10;

        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);

        const position = Math.floor(Math.random() * size);
        let ring = test_util.GenNewKeyRing(size, keyPair, position);
        let ring2 = test_util.GenNewKeyRing(size, keyPair2, position);
        let s = sign(payload, ring, keyPair, position);
        let s2 = sign(payload, ring2, keyPair2, position);
        let res = link(s, s2)
        expect(res).to.be.equal(false)
    })
    it('should link two signatures that are created using diferent payloads but same privKey', () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        var size = 10;

        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);

        const hash2 = new SHA3(256);
        hash2.update('helloworld2');
        let message_hash2 = hash2.digest('hex');
        let input2 = Buffer.from(message_hash2, 'hex');
        const payload2 = Uint8Array.from(input2);

        const position = Math.floor(Math.random() * size);
        let ring = test_util.GenNewKeyRing(size, keyPair, position);
        let s = sign(payload, ring, keyPair, position);
        let s2 = sign(payload2, ring, keyPair, position);
        let res = link(s, s2)
        expect(res).to.be.equal(true)
    })
})

describe("Signature computation", () => {
    it("Should produce a valid signature when the key is in the set", () => {
        var ec = new EC('secp256k1');
        let privKey = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        var size = 13;

        let keyList = [
            "045cbb56ac7f719a4950f2bb32144e02d987b67c6ebcda6c69daa7336d60aeea30197c32ebdc0cf7a4f43365c0d118e75b5acf5886118e1aade9330f118c5e4e9c",
            "043f4f74781dd9c87258347d64d0726d958061daff78d5f9a763f27cb2cd0588c8e08171bceb6fcb62ef8585f947ede9b8c5f82ad72a067d8ba827fd8710a1494a",
            "0405e62c631d549b7e162b02567265508c70cc99f30ca3754e94119ca981ac312c33c47c573e208968d9ce3de17f31ce47d718746a8cef66d1904f1e90b66da700",
            "0442e440a0054e88deee5b4039d08dd2407a3abe39f01cd5864edbec91ac8bab527ffe79e635195a7557cfc81c28721f8d621559687435ce11ee9f9478d9f78a10",
            "04d701d47dd31280cdf0ff791cb4d15a8fd9b6c4b77d1dbefb0d77d70186b8bc8f74cf446d1876607c2f2bcdae2ea5169451f97e5c64ed26787eca701e48b05f62",
            "04aa3988a2736e02a6de25e2da96bd8f045ae81312453a7ce098a71fa8b86516c9ab01e685cdfeb389f23ef9e7e9aea96aa97318c71de71c17784e3a0122695a3e",
            //"0423616e3f4009fe3cd63a56e21b898e03c9eaa716974555079541e3593a30c7dee9529c589259de7983fe03c3d75be669fac27974393a783f6cf4c742c8aa6cac", priv2pub
            "04883e1450d9c3068eac73f7e9f136521db8ce351fba4a1739a3cbeb6800172621c862d8ef12104586bc5fd0c9ad23c5f23da32ce99c4daac4112369f84412e069", // privpub
            "04a2f3c17b731688494577d8baa393d2f362cc4d876644c5998db299a419e2f1123889e1d1f322ee1800cbcaf7a54f776a9489baa935798c3f51d5926866984a27",
            "045cbb56ac7f719a4950f2bb32144e02d987b67c6ebcda6c69daa7336d60aeea30197c32ebdc0cf7a4f43365c0d118e75b5acf5886118e1aade9330f118c5e4e9c",
            "0476b27d34747b90f0fb4f8b49f6c7a2184d00d2dab72d15f34894060fab77e84222d4539365dd0e94729da01bae2af29a7847ac6fb34112506fbf1e2731bff220",
            "04f79011bf20e814db1330a6db981de7f4ab8e666d730fbbcd2df11ed8622773ed030141783ebc9075c9816d64c6a85635ea050d7c28b855244b6586e5c8db39ed",
            "0402309ddace73d46c02ebf66cd58bcce761a1a4e7a6e23415fa066be4298eb6c17e2caf238502d06a0aa8bccd940f80b202a4bf9db6e714a892bc8be144c075d1",
            "04115f9c0c5b9bf7221ec0d6791a1f77c02057bf9249e73b6c60618b434294f6cbd9430df2e5b1d25c49912ec1797bc4818c9e0513292a92e429aa22285bedd861"
        ]

        const hash = new SHA3(256);
        hash.update("helloworld");
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);

        let position = 7;
        let ring = test_util.GenNewKeyRingTEST(size, privKey, position);
        const signature = sign(payload, ring, privKey, position)

        let isValid = verify(signature, keyList)

        expect(isValid).to.be.equal(true)
    })
    it("Should throw an error when attempting to sign with an extraneous key pair", () => {
        var ec = new EC('secp256k1');
        let privKey = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3804", 'hex');
        var size = 13;

        let keyList = [
            "045cbb56ac7f719a4950f2bb32144e02d987b67c6ebcda6c69daa7336d60aeea30197c32ebdc0cf7a4f43365c0d118e75b5acf5886118e1aade9330f118c5e4e9c",
            "043f4f74781dd9c87258347d64d0726d958061daff78d5f9a763f27cb2cd0588c8e08171bceb6fcb62ef8585f947ede9b8c5f82ad72a067d8ba827fd8710a1494a",
            "0405e62c631d549b7e162b02567265508c70cc99f30ca3754e94119ca981ac312c33c47c573e208968d9ce3de17f31ce47d718746a8cef66d1904f1e90b66da700",
            "0442e440a0054e88deee5b4039d08dd2407a3abe39f01cd5864edbec91ac8bab527ffe79e635195a7557cfc81c28721f8d621559687435ce11ee9f9478d9f78a10",
            "04d701d47dd31280cdf0ff791cb4d15a8fd9b6c4b77d1dbefb0d77d70186b8bc8f74cf446d1876607c2f2bcdae2ea5169451f97e5c64ed26787eca701e48b05f62",
            "04aa3988a2736e02a6de25e2da96bd8f045ae81312453a7ce098a71fa8b86516c9ab01e685cdfeb389f23ef9e7e9aea96aa97318c71de71c17784e3a0122695a3e",
            //"0423616e3f4009fe3cd63a56e21b898e03c9eaa716974555079541e3593a30c7dee9529c589259de7983fe03c3d75be669fac27974393a783f6cf4c742c8aa6cac", priv2pub
            "04883e1450d9c3068eac73f7e9f136521db8ce351fba4a1739a3cbeb6800172621c862d8ef12104586bc5fd0c9ad23c5f23da32ce99c4daac4112369f84412e069", // privpub
            "04a2f3c17b731688494577d8baa393d2f362cc4d876644c5998db299a419e2f1123889e1d1f322ee1800cbcaf7a54f776a9489baa935798c3f51d5926866984a27",
            "045cbb56ac7f719a4950f2bb32144e02d987b67c6ebcda6c69daa7336d60aeea30197c32ebdc0cf7a4f43365c0d118e75b5acf5886118e1aade9330f118c5e4e9c",
            "0476b27d34747b90f0fb4f8b49f6c7a2184d00d2dab72d15f34894060fab77e84222d4539365dd0e94729da01bae2af29a7847ac6fb34112506fbf1e2731bff220",
            "04f79011bf20e814db1330a6db981de7f4ab8e666d730fbbcd2df11ed8622773ed030141783ebc9075c9816d64c6a85635ea050d7c28b855244b6586e5c8db39ed",
            "0402309ddace73d46c02ebf66cd58bcce761a1a4e7a6e23415fa066be4298eb6c17e2caf238502d06a0aa8bccd940f80b202a4bf9db6e714a892bc8be144c075d1",
            "04115f9c0c5b9bf7221ec0d6791a1f77c02057bf9249e73b6c60618b434294f6cbd9430df2e5b1d25c49912ec1797bc4818c9e0513292a92e429aa22285bedd861"
        ]

        const hash = new SHA3(256);
        hash.update("helloworld");
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);

        let position = 7;
        let ring = test_util.GenNewKeyRingTEST(size, privKey, position);
        const signature = sign(payload, ring, privKey, position)

        let isValid = verify(signature, keyList)

        expect(isValid).to.be.equal(false)
    })

    it("Should generate 10 rounds of 100 members and verify signatures coming from one of its members", () => {
        const hash = new SHA3(256);
        hash.update("helloworld");
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);

        var ec = new EC('secp256k1');
        var keys = ["043a6dea3371f721015ff58d7f3e6f454261cb4d6af9d3be98eaf36104076722351d016e61b749339f98cd355f72ee041f6c5ac5d13ac89cceabb8f5b9f5677b20", "04cd205d729fa796ab658b85d53c15673df414cce04aa4cc25fa50366c7af490819bc96b0f6d47ac99ecee8e4139017b9230c98b3e944896f00593d9c86bdd6398", "04fb8b5c2c4c12e64a24eb852a07dbce45d5fe9953558dc649c5da3a8c3ac2adba705cd5761a750e02a3bf050600f38230b8db17d29d0a7afc50c6bc32456c7486", "04a5fd1e6837dee3c0f1cfb0cdf871dc550a4d7492c1499aac019a486e87489c4b3de5124a24df1523c9a0d979eace965c7bf4b7af9b7e0bdc7c95cdb55b18a117", "04926e8547e62f0edc9e6f1b1090fb617b024926804203ce98758e7745a58ecb9e2dcf07e651569ffdaf747025d3d4ec903b540922e6a69eefca90f2a9ca3f2091", "043e3dd277a3d5b2279a397a939150f5ae091bee163bbdbb1b73f64e7c7a5fd794fe472da11bd02ce152308a35cacfea58996bbdc0e3065569583a2fcad748622e", "04155f37b6c34cb8ab0089dbae78c2c6ffd5f48d771cbce11c8d24e6f12e07d99229dbe22aa30e6278906d19759460cad985c61e7db7afe02f63064c5592f856a7", "04ce7178ab6dedc3f89b34bb6594949f5da90ada726586409b1545e3465c94f09d9affd60c47dc6f6ae8408309dc0febb5ef7145d8461a6c642e4751e9c8871097", "04fd70ccfa43144655c670713aa84dc51d2c7d3dd321a997ff53054ada1abfba9f6b7ca357573780f53a71fe49f3ddf9eb9559077179967be18dc94eec6696f4fc", "043994f3546e25581742d525b38ed763ceca499760fda42d9b01fcd70d5d2d26479ec96700381d9d2fa06d3cedb49d26d558d4ae5e61c815c71c161442dd4bafc1", "04560c638f86a84186b0aa178fac41f13549858f61005b992810b50eaeca2b56dea0779358d25e352708aaca96f3e91ccb85713de80c57f7d65307bd2e23d5c01b", "04d0f58817bfe5e87cc856b18d7b3ceedbda53860460bb66491e5d931d8e666bd8627ad441395ca98d70621e2eb602cbde40c456c97c6f04177454261c076512ab", "04410496fa1b7a5861959c8fee3981f84298b61a0334fcedac8c2cebd62d32caecd49e79ef293bb95247b5fa54de65880c649e55e5aae41835a75b8090c9421567", "04763a3b358f8d6caab659162947f941a5a677b4780d6cf1359508ffb30d2382e7649d3dd635ce621f35c690e58289352389d9886dde6828ba282cf5a8dc3bad37", "04cef040de3034ac3b8d039174ffb566d78254e4ac2282d6a56f9e257207711b01507378af7abf64f77150f2ca1da4768250d419194c36124603f464960bc1e149", "042a7ea7534dfb25663dabe6c8a1cafff5eb1a993650fccd1369872515a93df220a78258990321ba37797a502d4ed4bda936534201dba05b6897393ddf78d7ea24", "049acbce50fb1fd760096d1db10da5e888b7e3e1839035da76469f2d7c48978fa7238b214079d20a43ec2a2ca3e2a75365fec00a383da0fe17a17c226fafa99631", "04a21cd452b2b2432a856232ab7857c7e52a6a6683a2f71e79578f526d44e64e18d8aa534ed6c78667ceacf6b622bac660852ceadf966bb60e9613c691e3614f70", "04518a98de95fcbb0b7e93dec6956f1bd8248ac5f1fd92d69c4a23a8c821ced9b3e669d276082f190f373e0ed87d08a15d4021ce0f50853bb925172e40185944fb", "042fb12dc53282ee1b28421811890361d42e6444f35552a6accd5ccf65a570dc22d34a6ba6cd9ae3ff2ad52c06339197017e5bb41fac9315fdd5ac7578a0369c6f", "04e6fd228e039ada142f69c655f25d60d668c0dc3e1936a96a6daa3b9624c125ecd71ab8d56877a02d5af12e090823455ea43c23f65a1df7d9c1da236b973f3f5f", "042127c1f184d2fe459f5d0fb816e5e64ecb02b38f3edafef1b643b8853d243c039d7a9971e14218fc7db8f1e66ec0cc774c855c74faae610c27ad9bdda49bf0d5", "0403e816214779126480d0a6e015d6d05e136157ddffacee75dd48db6a92c5d52252ac2a172b45b9683430b05e8e361016bdd1564295bb1b9da17ba9fc3820eca7", "043038d1580a72d2e15c5b54b949de50661fa220fc96b64f2a269eb20aa2753e4f707e3b48936d16e7f2d1274e3ea14a036b8efb8798b7d9b9d67651bff9966907", "04b6255926233abc2195a811e68b302c201e2f43f32ed002ac6cb6ba03aad6f895a35a346baf13f2061791eca2eb81e6998f1f9665141148a874be2dbdd03b9aeb", "0471e096fe322a7e404c9c9bd0ffbc06999d091440939eda422986fb9bdffd52679cf185db2ea7d689d775320ca05944ac212471ba875845c6017c3b28a5eea8aa", "04b898352e924fab7fed8834e70f69db14497dc042af123419cb36577012100422085d44200ce4913c38f5fc5e9c6bd25821e9237b2fb314cf2eec476a56fad526", "040e37610513458609390f566b36015498e526ce72c39412ccbc8297caa00afc0224f05244796e865c053bfa5e51eae4c9852b9d9af8a3f50b7c176b4ecf765cdb", "04221391708deee17690bc905006f552f66cfcf84033140291e6e8b58bfa7100707b13a7016cda2c909a2e91ee109bb258c14195f3b02b613a17232f46c8604402", "0407d095661b6ae499511d98c07c37c442b2a7bf07283be064d1ba35d657d7ccdfda47056c1e3927a501ba365e6b6b2d1e3c2ef25c1d20cc7ef71455336ff2e358", "04338811ac6954c6a056a10c34f6982c28f7a983033c2d1058320d745ddbf9ebd7caf3bac55e3d9d50d62f1a8d7baa23f9d88f263dbd7b56da3c77e16e5509d1d4", "04496c599245f206ee98043df3d752b7d466762e5a1edbf6bf60035db65603ee878d1fe3364983c2781be95563694aa2e92c1efda0781de83addb0492be1afeccc", "0405eae736c93e97bdc322dcee16890f5dcd934324bad0821b3a5f2063c2e9c2f1ab285e8bbb16198e227a0063bb8106945b40259f93bb7a7e1d817ec7ec1a1c56", "043dd8465a25714bf83836d0984d48446baeb8b8269fea9095980f4b078960f89f69afc0de0b65ff2afc14634d0163d022d6a202f5825ddcd22d8437205f0200c7", "045abdfdeaecbb616f4da7da30162e5b298a77b2bc2db9cd45d05df71d7d2b8fb7b65484921c34c852d83af5f122ef2e5323b31c5277ea8f77a0f1696565509df6", "04ee76ec7b7b6ba25c89828fe6191af4f97a7d98fa4aa9ac9b5bb846686deb7ffeafcaeb310c6f1c942aa3f29e9400584a997e49a9385b990e24bc8099d39d45de", "0458f5821126a1b1880430350aeeb04af5800d3b789df3c27d6db67c169abcf00bbf099de355735c9806b1dab66ba1a25739721787e3d0afc9f103ace7cc48d0dd", "04033bdb90135aeec236b0888cec2c2cc0a16fe47e7256056e66bffb6fa4059e8812ceb5c1cbd9d5d86b10a27364a09e3b90dedf0cbb444ad215b713fdbb8da300", "043f6c43b0c4097749e4aba11cf3f9774b341a19060c91e5dcbfc8304d013914f25e6fef93f5d85de4e57714196107929bee1a55415a7011088dea7bab3f00f211", "04c309c00212154e98bbdf974168efce5d445dd7cafd1302a16bf592917cb0c1443b4ee72f4bae5a59dc62c444c5e1f788ad3c68f87ea7b4bc08922bb998abecf5", "04189a55d7cc052f492fb4af28743a3c322e3a0ea094d83e8403d546560dd03fa6246d348a24dd839d7384809f28e4215a7c176d9224b421ba01a6818b03a60f61", "04c1d9ae4ce6006406c87021286ab209eea76c3dc3b0bc918f7f2fc611c5319f1ce6390f306d65a7eafd986fffaf8467121647d865d9b2fd10bf17b9dbb7103661", "04da7fdc104540a9848217eeeb00f951860bac3428e9e642e29c26dcd7ac412c624aff1d4e9a79947691e78c5a57bbfba9a266dc5e649e5a0ed35f440a3895dcce", "043f580eeddad524841a4bcd0b0da125a3e88380fc1636761a4d4c1ae47bb92cb7ce4f89a703479cb2983538a0896e5980f826ce432c2380fe07c8fb24a2865b84", "048f81f67edca88ea163af74a15bba1d5b5f8259b5e9dd9401385e312d3b93b362187985ebe3da3c99b8886d0b88ec7ab581c6896ee264119416a55f719c5e5141", "04a6f288e47fe7d77b75891b712433dc3177faa1c96eb922bd84f1bb362fd77179a747f609d45e3f0eafb0143d7dea6bdd083206fdf03f13221e9ca2d7c6d4b7bd", "047265767b067f1a656e8a6d38579e3c3f00856f0f0b16008cc226390ac793bc61a929a7ef80b9b112aad07f5d36e378e6f167b1e972b7360210e073ae64ff43d5", "0458714c48d6154c9e6de40b8dec8e54f423b09bbfd639bea2ce386545713b3ac5f960ffc62393405d55ddc0780226da23b61c9d055d7e2680e7de1fc64e84006d", "042e9a7f0462dfe4029ac79a28818995ee7375121c795cd3ea3bef966f319d9302f5589688ee57484f896add5ff6b0b7d0149d508f9f5a2fd207c263efda56186c", "04e6dfbd18f2edb054655f0ade24962ec4c49e4f7379ef5bf3a18d1c41f8dd870aaf3d1b7501c7cb2981af84e1a783c921c77e0dfe195dce38dddc4f79b9010e78", "04011362a5a21746ad40013a5ac49d7d3308aef71b0dfa65d50c97f7b7645b0f7281bce19126a06459546852a6b602482169e0038fa286f6b98dce05348b633cd0", "045cfb90971db2e8f65f636a445a9c5706a174ba1bda8b576a9538b1b6b322a9ae3a4a3bb7179bdd2ff86479d4de1fe4a282ac96065c028b161a79b9ef429b554f", "04e6683d2b7a9e715810698cd384a61585f382a96e32372328ff1bf4632b4119a0d836ebca7ddc6e337c8adfaa2275c44577c6ab4888f3624afaabdbf3820ab581", "0486a9c5c33d718f13f4a5baca6dae2f383bbe605e114390693f7d628f43fde21f19a613d26fddb416e50285f9720df48314470cfeeca13780f146454757ef8f67", "04d5e676463b6b691cc91a2cfbcb62356bb4c7d9b55d3a64957c5ccecc917792489d21b6b6840df29a87ebddacbaaea0292740be4813d854b2b12397d54ca45278", "047feb0a6d723df4851dda7e1c0a751dea2b955745de3733d6c62c3fde99c4c0563334f9ed9f6b046890fe92637ee6a7af5f17da81b295c433b4487cef30ea260d", "049b9effa1f410bbda3dea2da48e80f97a92484e295ce64392318090c02976fc4d8dd83bf25bcd43d8ffefee0d12c57d36f5dd8edf13faed7c765a4ac83763a9fd", "046df362d1ced83db49495b17010dae4d614e7d47e761f63a01776594336913f23e47bd14e0ae3a3b2e19687c1e1833bb94e717d3970538b1916db5e383ba93714", "04af04ef033e2f85d728de8af613b24d35610186d7ce9f69e0e7b293e6261a210485c242702512a523cfa6a120c43cd1c1fb367f026f66ddaa75752122345a076b", "0409d84a2e4fe57c5f76b46c2ea0d29c683fd4e99f9534035c5b3e5d372f79c67edbf0e751714fe81d8c4d50b331ff47ab4f013d18a3a7df12582e2d1cafd99920", "04d52a056957c5745ad72fe91dde81a149b84fd1387c07da18b6913ba299d45aa96f9f0f1e8ac7929f335c43341fda469d3a86fc8a9aa663f59e01f7bd25511005", "04b87a767fd49a5b06798e42e1fa9c551a314dd3f6a7a3f3516cdfe1bc921e6877f255d437cf3c89391da7f7487cd1e6b6c0e002126475b52aa64b058e9a8f6a05", "049d762185153aff83eef98404ff9662d9ef957d6debc76f7d3b7fac2793412b6f61b14028fc9236f08f54e8470d471ebcf5966d65788e8169e1fc318a899d85e4", "04bbb0fd447ee39ef4690f572974ffb0c27580167055727f9aab701bff9d815adc4cbd9c0c9f0d40cd835f6d92d0c9f840c4b435cedf02672076061d8e8261cfe4", "04f027e59c4a1f4b2eca66a671946b8501276ec34e46e775363ac9efe9c36f63a0172d1f7343094f6d62ee00e8443018c181dc1e4c6bea451a52dfd3bcb058d8aa", "04771b714e17085df8ece32e58258cd62731549cfa1c6f03927662a666766557d4e1dd4570fa792aca1ef9215bba1346534cdfa0761c4faa808ac4e6629de6bb9b", "04ff2daa13520cb0f7f82101c677c4cf91cd9c1394b4ceec4e5b38b7c315d02fbe9c775fd942b7705a6c061366710d4aa778b78c42966af3d9341ccf1f93ae7998", "044b1d34681a153f9d04ed4adb4a15888446c8d5b9f5223d14b39bbdf22b766e6a6bb7c73b0a5f799031f2c8babe729427b29b8416a65f9b251ae8e812f5140e3e", "0452a29f7723f82a536cae05bc1cca00f63654a2399344dadb46968123f23ba764ff86702449e47da6e643f20aeaa44d7a390ea2da60fbfa487709aa5191deac5c", "047db3b4a0ccbc10c43f51a33d1a52b5863af2b671f46a981a66326bc9b15d18086822da418d8350dce897c72e7d467742ed89f7a0bc8f04c82fda50fdda68b439", "041794c145862c15441da685864757dcec01cc33aec6adbec2980f05882fc60751a7724327ea73ee80470e48fdf40fef703b04df76a0f55cd230593fba7ed6615d", "04f84f61b9a73f52d4e4a4f02deffa64da6e128ed2a5ee7d6041e9746caa42a5632313a3c7a696fb17f8f6e8b6b0d1ff42d28b518e51fe4b60444dcea46f358bb5", "0442f8638976c7c2b65bf27c160994ef7f39640afa4ecfa3ff9affcc563d9b720d8594764cfd7e1834a728cb2b7e54df922ed5e16bdcf08fb8f4e984df62bee50b", "04937246024159b30e83afdbbdbc30ae45cd511990ced4054ac9912b6150a997a1ce86a5c7ba10da0be682e3dda983915d51489394c381401acb0dd66247d74fd5", "04ea717df00bf9372dc431cb24df79a87135d94295f06ee4b7fc4c17dbaf92ed69a2e5d58a730f93a0f9f5273ca5cb17a08ace40a8adbad27d4e44f93ab9e6933d", "04744ee60d4638d6fecd69e82436d43cb562620f0b79c4eb20f50d7f819479a3fcb3f7c452abf951c04537171e99042b4cbf4768127c34201079deab6e53ff186b", "0419ac806bb2a85be9966cb76d3bf2825fdc84dbd25a410939504abeb98e7a5ed8d8fbce5d5b7fb17adfaa0097a1874fc81297f45f43453862e1bd998232b9f603", "043f498c983fcbb558906992c67a8e8154911305170395293ac014edd06745e030c39a305f2087578eca285f599e1105ba094051472c2b280027f817ed33d9f3a8", "047f196bd05481af5525ccdc4abb328835bf5f51dfc967f49c5989588df937ea1e12567f07c9c752c886c0f3eb38e8a09dd9aa61939909b331717cd9eca7f5d70f", "04ad8cb36355917b96dec6ac2f091063e62edab836ea09435c7e37db5590ad1a00116f549b414b9115833a76576af14a2cdd5ac2e42680b8d17e3378b35f8bbf67", "04dad394b119079d207f8678fa62c92d3613f2461e4c8112d88503986861b409e957c9f69be59d53009a52bf95d26e44ac8147de2273700c2ad1babffaf488ec45", "04e37e5cf57a094ea8bc8273dcc63cb5debd7255f759d286d33fc26929eb980c8bf3461570a699439247fd5336d464b32442dac44ff5ac90051aa8bf2ef4e513f0", "04bf37fc6033833b8cc0f64181ecd7afd3c7d75bb0d2b0050bfe88116a60968144008e5505b48755281a730009898fa84ce84aaedd1f25e8e8b1441496ed30cb48", "04ef92681a7c8758bb02491f817fc090e319d679e8e3413edcfb32587217caf7e05fd166da57bd324fb0182e9d5ab887ecc7ed7f55389d736b02c3ec33994ab4c6", "040dbd6e2e2bf27332ad3d798fc8de4d49b05f83f69af4d4f3bb3ebf9434e2f7a6bf9007454eb6a3142a4ca29dc94ba32fe8d8a1cbc542205ec8e5ab3294d7b453", "04a275f18b40f252d86c5fa3fac60c9fe691b53e176922fe7d9123905667587b8f2d328440c23cae7337648388b6dc87a61251d4ba9e7062bf472507e2c69cae13", "045a6b8c47ff17583d04e83451726bacb8b4657f6933c8af18e29005719a9666ec152ab1b04a76d5f64a2733646dbd31e59e186a750608a3809616f9b1dc6a4031", "048dcc24817010d4b8290fad4925a60fe971d6f60c97868d848abc8ac00945b8126dea274a6d68f6504ddeec5533dd72bef4c7a0636c9b9e4a4e4102469e4e5415", "04c081a881e418d3cc6bebfe9468519ec1160a9e500d8d26a0fabd83f631adf465c5119241ff57f2694f6d600dd616ae9fcf6b375adf459137c11d90ae3b1dcb8f", "04bb79e2f57105df86e08ed586fddc1436a9d8a5ba1b5d964467ad0b8718ec8f2c689518ee39adbf2b9e132a563448bb153b206909eefccbce4a7f6eedd40e9550", "049476fb87747ade781a0e3ec46efc371f0e75ba551a7aebcedcd1b5b9376df40cc27b9215f63b6b58400af066c03c9b9542f496ef4c8d2b361812ba7f52a480f0", "04e255f23f5fbd18cb2bc6550d5de03d5b89e389431979053b3dc83e6f74e4340caee379bdf6ebc318874f5d0dfb930f0ab6a03d9675fa480a675131f98f6da30b", "045a279e92b1e9a9b1f07e65c0c6c8f265679108b2e857db2eccef5558624f4f198153f20ccfaee7c916662c9ffefc2a8ab9faa27bd0e0fe7068033336a097e554", "043229035f1757a2ed69a63d4f041ce708d04f73182b0f792bd2b8ba0292dc4ef61751b30e2a148cd179c06b2513dfde177c0ab46b6b4d5a0c172da39019adbfa1", "047cc5702707175a0574ac93a6028218eaf59620100f30d026fb03a8ae1b673f4ab1efcba56dcd531f03b5d3567a9b3ef39618c139f199194c21a2ab4b057d0216", "041374067e5bde41a6edaa8f6635fc2beabf267514a0182b4c0d310c3f2d25e2d70e7bed4e5922933690d0e530210142ea1fc6da7b329d01bcb7835bb1e6224c48", "04dfcded487b4de10a9b2d33f1f0374f0ea09fe08f63dad07c4b99f4054faa30156d8670332cc87362bd2b6057e96b5af3dc49a9a67fdb0ed0d0aa19d39c3dffa6", "04579bfa77a6366a29e85ec5bbac02d72817c9638b8fc12e5f39b20571f20425f6b44b6d4a5b58e27162f11dd2c88885135edbb42af6b0628f7a98390466c1d5b1", "044efe402dbb86e8732e1ff6d07cd43ca4553151e48490e1a5844f491a72d753bdf934c9441fec5ed7d99c7df2172da4fd25bbc97721efe20da84617672c334a0f", "043eb86aedc3d1eaa5aeecf3c52b6478c638ce969197b32f39570c8061bfa2f88ccdcca958cc3eb2bd29c5e0403e7b771e8b265ef1d8ee6b31e68ad1ddaa8fe9fc"]
        // [56,10,0,99,17,98,34,12,7,77]
        var keyspriv = [
            "9bfee3f3e528e7380fa13ff781bd5141cd22e2bf1806267642f78da6269718e0",
            "ba2e27ef7c74d4cbc3141d71191caf15d513284076b94c2c2b6d180c0839da88",
            "8799196adeb0c2508cc184f90ad8b7ba6ff5a46ce670b4066e707520f32fdabd",
            "9baf1b911d121d250e5e5a793b05d059b3487f8ce408cfb360b0fb917d134bbe",
            "aa07226544abc12b82d764cc075bc185a2a579faeccea8753aec4b05d093ee2f",
            "c3343d9b7df963296b4c1c1656b07f256d5042971aa7e82b0bd933d9ed9e8525",
            "7cd93c2d17178c518359bd9ba336453c119002369603c70f8b968f9b13c10ee3",
            "3ee844a2d628190bf293a3d6ec68af3bee03f80286f7fce38156acbf740e4839",
            "a5ba4620fd302910ab7c9978048c9a8cb999a31f86c0d2fe1897d8783e5b3b95",
            "8217e4d29ce9a85ee8f9ea140c4cda605be75f709bd3eae94274f4823a46efa3"
        ]
        var positions = [56, 10, 0, 99, 17, 98, 34, 12, 7, 77]
        for (var i = 0; i < 10; i++) {
            let kp = ec.keyFromPrivate(keyspriv[i], 'hex');
            let pos = positions[i]
            let ring = makeKeyring(keys, kp, pos)
            const signature = sign(payload, ring, kp, pos)
            let isValid = verify(signature, keys)
            expect(isValid).to.be.equal(true)
        }
    })

    it("Should generate 10 rounds of 100 members, 10 extraneous keys and reject all of these signatures", () => {
        const hash = new SHA3(256);
        hash.update("helloworld");
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);

        var ec = new EC('secp256k1');
        var keys = ["043a6dea3371f721015ff58d7f3e6f454261cb4d6af9d3be98eaf36104076722351d016e61b749339f98cd355f72ee041f6c5ac5d13ac89cceabb8f5b9f5677b20", "04cd205d729fa796ab658b85d53c15673df414cce04aa4cc25fa50366c7af490819bc96b0f6d47ac99ecee8e4139017b9230c98b3e944896f00593d9c86bdd6398", "04fb8b5c2c4c12e64a24eb852a07dbce45d5fe9953558dc649c5da3a8c3ac2adba705cd5761a750e02a3bf050600f38230b8db17d29d0a7afc50c6bc32456c7486", "04a5fd1e6837dee3c0f1cfb0cdf871dc550a4d7492c1499aac019a486e87489c4b3de5124a24df1523c9a0d979eace965c7bf4b7af9b7e0bdc7c95cdb55b18a117", "04926e8547e62f0edc9e6f1b1090fb617b024926804203ce98758e7745a58ecb9e2dcf07e651569ffdaf747025d3d4ec903b540922e6a69eefca90f2a9ca3f2091", "043e3dd277a3d5b2279a397a939150f5ae091bee163bbdbb1b73f64e7c7a5fd794fe472da11bd02ce152308a35cacfea58996bbdc0e3065569583a2fcad748622e", "04155f37b6c34cb8ab0089dbae78c2c6ffd5f48d771cbce11c8d24e6f12e07d99229dbe22aa30e6278906d19759460cad985c61e7db7afe02f63064c5592f856a7", "04ce7178ab6dedc3f89b34bb6594949f5da90ada726586409b1545e3465c94f09d9affd60c47dc6f6ae8408309dc0febb5ef7145d8461a6c642e4751e9c8871097", "04fd70ccfa43144655c670713aa84dc51d2c7d3dd321a997ff53054ada1abfba9f6b7ca357573780f53a71fe49f3ddf9eb9559077179967be18dc94eec6696f4fc", "043994f3546e25581742d525b38ed763ceca499760fda42d9b01fcd70d5d2d26479ec96700381d9d2fa06d3cedb49d26d558d4ae5e61c815c71c161442dd4bafc1", "04560c638f86a84186b0aa178fac41f13549858f61005b992810b50eaeca2b56dea0779358d25e352708aaca96f3e91ccb85713de80c57f7d65307bd2e23d5c01b", "04d0f58817bfe5e87cc856b18d7b3ceedbda53860460bb66491e5d931d8e666bd8627ad441395ca98d70621e2eb602cbde40c456c97c6f04177454261c076512ab", "04410496fa1b7a5861959c8fee3981f84298b61a0334fcedac8c2cebd62d32caecd49e79ef293bb95247b5fa54de65880c649e55e5aae41835a75b8090c9421567", "04763a3b358f8d6caab659162947f941a5a677b4780d6cf1359508ffb30d2382e7649d3dd635ce621f35c690e58289352389d9886dde6828ba282cf5a8dc3bad37", "04cef040de3034ac3b8d039174ffb566d78254e4ac2282d6a56f9e257207711b01507378af7abf64f77150f2ca1da4768250d419194c36124603f464960bc1e149", "042a7ea7534dfb25663dabe6c8a1cafff5eb1a993650fccd1369872515a93df220a78258990321ba37797a502d4ed4bda936534201dba05b6897393ddf78d7ea24", "049acbce50fb1fd760096d1db10da5e888b7e3e1839035da76469f2d7c48978fa7238b214079d20a43ec2a2ca3e2a75365fec00a383da0fe17a17c226fafa99631", "04a21cd452b2b2432a856232ab7857c7e52a6a6683a2f71e79578f526d44e64e18d8aa534ed6c78667ceacf6b622bac660852ceadf966bb60e9613c691e3614f70", "04518a98de95fcbb0b7e93dec6956f1bd8248ac5f1fd92d69c4a23a8c821ced9b3e669d276082f190f373e0ed87d08a15d4021ce0f50853bb925172e40185944fb", "042fb12dc53282ee1b28421811890361d42e6444f35552a6accd5ccf65a570dc22d34a6ba6cd9ae3ff2ad52c06339197017e5bb41fac9315fdd5ac7578a0369c6f", "04e6fd228e039ada142f69c655f25d60d668c0dc3e1936a96a6daa3b9624c125ecd71ab8d56877a02d5af12e090823455ea43c23f65a1df7d9c1da236b973f3f5f", "042127c1f184d2fe459f5d0fb816e5e64ecb02b38f3edafef1b643b8853d243c039d7a9971e14218fc7db8f1e66ec0cc774c855c74faae610c27ad9bdda49bf0d5", "0403e816214779126480d0a6e015d6d05e136157ddffacee75dd48db6a92c5d52252ac2a172b45b9683430b05e8e361016bdd1564295bb1b9da17ba9fc3820eca7", "043038d1580a72d2e15c5b54b949de50661fa220fc96b64f2a269eb20aa2753e4f707e3b48936d16e7f2d1274e3ea14a036b8efb8798b7d9b9d67651bff9966907", "04b6255926233abc2195a811e68b302c201e2f43f32ed002ac6cb6ba03aad6f895a35a346baf13f2061791eca2eb81e6998f1f9665141148a874be2dbdd03b9aeb", "0471e096fe322a7e404c9c9bd0ffbc06999d091440939eda422986fb9bdffd52679cf185db2ea7d689d775320ca05944ac212471ba875845c6017c3b28a5eea8aa", "04b898352e924fab7fed8834e70f69db14497dc042af123419cb36577012100422085d44200ce4913c38f5fc5e9c6bd25821e9237b2fb314cf2eec476a56fad526", "040e37610513458609390f566b36015498e526ce72c39412ccbc8297caa00afc0224f05244796e865c053bfa5e51eae4c9852b9d9af8a3f50b7c176b4ecf765cdb", "04221391708deee17690bc905006f552f66cfcf84033140291e6e8b58bfa7100707b13a7016cda2c909a2e91ee109bb258c14195f3b02b613a17232f46c8604402", "0407d095661b6ae499511d98c07c37c442b2a7bf07283be064d1ba35d657d7ccdfda47056c1e3927a501ba365e6b6b2d1e3c2ef25c1d20cc7ef71455336ff2e358", "04338811ac6954c6a056a10c34f6982c28f7a983033c2d1058320d745ddbf9ebd7caf3bac55e3d9d50d62f1a8d7baa23f9d88f263dbd7b56da3c77e16e5509d1d4", "04496c599245f206ee98043df3d752b7d466762e5a1edbf6bf60035db65603ee878d1fe3364983c2781be95563694aa2e92c1efda0781de83addb0492be1afeccc", "0405eae736c93e97bdc322dcee16890f5dcd934324bad0821b3a5f2063c2e9c2f1ab285e8bbb16198e227a0063bb8106945b40259f93bb7a7e1d817ec7ec1a1c56", "043dd8465a25714bf83836d0984d48446baeb8b8269fea9095980f4b078960f89f69afc0de0b65ff2afc14634d0163d022d6a202f5825ddcd22d8437205f0200c7", "045abdfdeaecbb616f4da7da30162e5b298a77b2bc2db9cd45d05df71d7d2b8fb7b65484921c34c852d83af5f122ef2e5323b31c5277ea8f77a0f1696565509df6", "04ee76ec7b7b6ba25c89828fe6191af4f97a7d98fa4aa9ac9b5bb846686deb7ffeafcaeb310c6f1c942aa3f29e9400584a997e49a9385b990e24bc8099d39d45de", "0458f5821126a1b1880430350aeeb04af5800d3b789df3c27d6db67c169abcf00bbf099de355735c9806b1dab66ba1a25739721787e3d0afc9f103ace7cc48d0dd", "04033bdb90135aeec236b0888cec2c2cc0a16fe47e7256056e66bffb6fa4059e8812ceb5c1cbd9d5d86b10a27364a09e3b90dedf0cbb444ad215b713fdbb8da300", "043f6c43b0c4097749e4aba11cf3f9774b341a19060c91e5dcbfc8304d013914f25e6fef93f5d85de4e57714196107929bee1a55415a7011088dea7bab3f00f211", "04c309c00212154e98bbdf974168efce5d445dd7cafd1302a16bf592917cb0c1443b4ee72f4bae5a59dc62c444c5e1f788ad3c68f87ea7b4bc08922bb998abecf5", "04189a55d7cc052f492fb4af28743a3c322e3a0ea094d83e8403d546560dd03fa6246d348a24dd839d7384809f28e4215a7c176d9224b421ba01a6818b03a60f61", "04c1d9ae4ce6006406c87021286ab209eea76c3dc3b0bc918f7f2fc611c5319f1ce6390f306d65a7eafd986fffaf8467121647d865d9b2fd10bf17b9dbb7103661", "04da7fdc104540a9848217eeeb00f951860bac3428e9e642e29c26dcd7ac412c624aff1d4e9a79947691e78c5a57bbfba9a266dc5e649e5a0ed35f440a3895dcce", "043f580eeddad524841a4bcd0b0da125a3e88380fc1636761a4d4c1ae47bb92cb7ce4f89a703479cb2983538a0896e5980f826ce432c2380fe07c8fb24a2865b84", "048f81f67edca88ea163af74a15bba1d5b5f8259b5e9dd9401385e312d3b93b362187985ebe3da3c99b8886d0b88ec7ab581c6896ee264119416a55f719c5e5141", "04a6f288e47fe7d77b75891b712433dc3177faa1c96eb922bd84f1bb362fd77179a747f609d45e3f0eafb0143d7dea6bdd083206fdf03f13221e9ca2d7c6d4b7bd", "047265767b067f1a656e8a6d38579e3c3f00856f0f0b16008cc226390ac793bc61a929a7ef80b9b112aad07f5d36e378e6f167b1e972b7360210e073ae64ff43d5", "0458714c48d6154c9e6de40b8dec8e54f423b09bbfd639bea2ce386545713b3ac5f960ffc62393405d55ddc0780226da23b61c9d055d7e2680e7de1fc64e84006d", "042e9a7f0462dfe4029ac79a28818995ee7375121c795cd3ea3bef966f319d9302f5589688ee57484f896add5ff6b0b7d0149d508f9f5a2fd207c263efda56186c", "04e6dfbd18f2edb054655f0ade24962ec4c49e4f7379ef5bf3a18d1c41f8dd870aaf3d1b7501c7cb2981af84e1a783c921c77e0dfe195dce38dddc4f79b9010e78", "04011362a5a21746ad40013a5ac49d7d3308aef71b0dfa65d50c97f7b7645b0f7281bce19126a06459546852a6b602482169e0038fa286f6b98dce05348b633cd0", "045cfb90971db2e8f65f636a445a9c5706a174ba1bda8b576a9538b1b6b322a9ae3a4a3bb7179bdd2ff86479d4de1fe4a282ac96065c028b161a79b9ef429b554f", "04e6683d2b7a9e715810698cd384a61585f382a96e32372328ff1bf4632b4119a0d836ebca7ddc6e337c8adfaa2275c44577c6ab4888f3624afaabdbf3820ab581", "0486a9c5c33d718f13f4a5baca6dae2f383bbe605e114390693f7d628f43fde21f19a613d26fddb416e50285f9720df48314470cfeeca13780f146454757ef8f67", "04d5e676463b6b691cc91a2cfbcb62356bb4c7d9b55d3a64957c5ccecc917792489d21b6b6840df29a87ebddacbaaea0292740be4813d854b2b12397d54ca45278", "047feb0a6d723df4851dda7e1c0a751dea2b955745de3733d6c62c3fde99c4c0563334f9ed9f6b046890fe92637ee6a7af5f17da81b295c433b4487cef30ea260d", "049b9effa1f410bbda3dea2da48e80f97a92484e295ce64392318090c02976fc4d8dd83bf25bcd43d8ffefee0d12c57d36f5dd8edf13faed7c765a4ac83763a9fd", "046df362d1ced83db49495b17010dae4d614e7d47e761f63a01776594336913f23e47bd14e0ae3a3b2e19687c1e1833bb94e717d3970538b1916db5e383ba93714", "04af04ef033e2f85d728de8af613b24d35610186d7ce9f69e0e7b293e6261a210485c242702512a523cfa6a120c43cd1c1fb367f026f66ddaa75752122345a076b", "0409d84a2e4fe57c5f76b46c2ea0d29c683fd4e99f9534035c5b3e5d372f79c67edbf0e751714fe81d8c4d50b331ff47ab4f013d18a3a7df12582e2d1cafd99920", "04d52a056957c5745ad72fe91dde81a149b84fd1387c07da18b6913ba299d45aa96f9f0f1e8ac7929f335c43341fda469d3a86fc8a9aa663f59e01f7bd25511005", "04b87a767fd49a5b06798e42e1fa9c551a314dd3f6a7a3f3516cdfe1bc921e6877f255d437cf3c89391da7f7487cd1e6b6c0e002126475b52aa64b058e9a8f6a05", "049d762185153aff83eef98404ff9662d9ef957d6debc76f7d3b7fac2793412b6f61b14028fc9236f08f54e8470d471ebcf5966d65788e8169e1fc318a899d85e4", "04bbb0fd447ee39ef4690f572974ffb0c27580167055727f9aab701bff9d815adc4cbd9c0c9f0d40cd835f6d92d0c9f840c4b435cedf02672076061d8e8261cfe4", "04f027e59c4a1f4b2eca66a671946b8501276ec34e46e775363ac9efe9c36f63a0172d1f7343094f6d62ee00e8443018c181dc1e4c6bea451a52dfd3bcb058d8aa", "04771b714e17085df8ece32e58258cd62731549cfa1c6f03927662a666766557d4e1dd4570fa792aca1ef9215bba1346534cdfa0761c4faa808ac4e6629de6bb9b", "04ff2daa13520cb0f7f82101c677c4cf91cd9c1394b4ceec4e5b38b7c315d02fbe9c775fd942b7705a6c061366710d4aa778b78c42966af3d9341ccf1f93ae7998", "044b1d34681a153f9d04ed4adb4a15888446c8d5b9f5223d14b39bbdf22b766e6a6bb7c73b0a5f799031f2c8babe729427b29b8416a65f9b251ae8e812f5140e3e", "0452a29f7723f82a536cae05bc1cca00f63654a2399344dadb46968123f23ba764ff86702449e47da6e643f20aeaa44d7a390ea2da60fbfa487709aa5191deac5c", "047db3b4a0ccbc10c43f51a33d1a52b5863af2b671f46a981a66326bc9b15d18086822da418d8350dce897c72e7d467742ed89f7a0bc8f04c82fda50fdda68b439", "041794c145862c15441da685864757dcec01cc33aec6adbec2980f05882fc60751a7724327ea73ee80470e48fdf40fef703b04df76a0f55cd230593fba7ed6615d", "04f84f61b9a73f52d4e4a4f02deffa64da6e128ed2a5ee7d6041e9746caa42a5632313a3c7a696fb17f8f6e8b6b0d1ff42d28b518e51fe4b60444dcea46f358bb5", "0442f8638976c7c2b65bf27c160994ef7f39640afa4ecfa3ff9affcc563d9b720d8594764cfd7e1834a728cb2b7e54df922ed5e16bdcf08fb8f4e984df62bee50b", "04937246024159b30e83afdbbdbc30ae45cd511990ced4054ac9912b6150a997a1ce86a5c7ba10da0be682e3dda983915d51489394c381401acb0dd66247d74fd5", "04ea717df00bf9372dc431cb24df79a87135d94295f06ee4b7fc4c17dbaf92ed69a2e5d58a730f93a0f9f5273ca5cb17a08ace40a8adbad27d4e44f93ab9e6933d", "04744ee60d4638d6fecd69e82436d43cb562620f0b79c4eb20f50d7f819479a3fcb3f7c452abf951c04537171e99042b4cbf4768127c34201079deab6e53ff186b", "0419ac806bb2a85be9966cb76d3bf2825fdc84dbd25a410939504abeb98e7a5ed8d8fbce5d5b7fb17adfaa0097a1874fc81297f45f43453862e1bd998232b9f603", "043f498c983fcbb558906992c67a8e8154911305170395293ac014edd06745e030c39a305f2087578eca285f599e1105ba094051472c2b280027f817ed33d9f3a8", "047f196bd05481af5525ccdc4abb328835bf5f51dfc967f49c5989588df937ea1e12567f07c9c752c886c0f3eb38e8a09dd9aa61939909b331717cd9eca7f5d70f", "04ad8cb36355917b96dec6ac2f091063e62edab836ea09435c7e37db5590ad1a00116f549b414b9115833a76576af14a2cdd5ac2e42680b8d17e3378b35f8bbf67", "04dad394b119079d207f8678fa62c92d3613f2461e4c8112d88503986861b409e957c9f69be59d53009a52bf95d26e44ac8147de2273700c2ad1babffaf488ec45", "04e37e5cf57a094ea8bc8273dcc63cb5debd7255f759d286d33fc26929eb980c8bf3461570a699439247fd5336d464b32442dac44ff5ac90051aa8bf2ef4e513f0", "04bf37fc6033833b8cc0f64181ecd7afd3c7d75bb0d2b0050bfe88116a60968144008e5505b48755281a730009898fa84ce84aaedd1f25e8e8b1441496ed30cb48", "04ef92681a7c8758bb02491f817fc090e319d679e8e3413edcfb32587217caf7e05fd166da57bd324fb0182e9d5ab887ecc7ed7f55389d736b02c3ec33994ab4c6", "040dbd6e2e2bf27332ad3d798fc8de4d49b05f83f69af4d4f3bb3ebf9434e2f7a6bf9007454eb6a3142a4ca29dc94ba32fe8d8a1cbc542205ec8e5ab3294d7b453", "04a275f18b40f252d86c5fa3fac60c9fe691b53e176922fe7d9123905667587b8f2d328440c23cae7337648388b6dc87a61251d4ba9e7062bf472507e2c69cae13", "045a6b8c47ff17583d04e83451726bacb8b4657f6933c8af18e29005719a9666ec152ab1b04a76d5f64a2733646dbd31e59e186a750608a3809616f9b1dc6a4031", "048dcc24817010d4b8290fad4925a60fe971d6f60c97868d848abc8ac00945b8126dea274a6d68f6504ddeec5533dd72bef4c7a0636c9b9e4a4e4102469e4e5415", "04c081a881e418d3cc6bebfe9468519ec1160a9e500d8d26a0fabd83f631adf465c5119241ff57f2694f6d600dd616ae9fcf6b375adf459137c11d90ae3b1dcb8f", "04bb79e2f57105df86e08ed586fddc1436a9d8a5ba1b5d964467ad0b8718ec8f2c689518ee39adbf2b9e132a563448bb153b206909eefccbce4a7f6eedd40e9550", "049476fb87747ade781a0e3ec46efc371f0e75ba551a7aebcedcd1b5b9376df40cc27b9215f63b6b58400af066c03c9b9542f496ef4c8d2b361812ba7f52a480f0", "04e255f23f5fbd18cb2bc6550d5de03d5b89e389431979053b3dc83e6f74e4340caee379bdf6ebc318874f5d0dfb930f0ab6a03d9675fa480a675131f98f6da30b", "045a279e92b1e9a9b1f07e65c0c6c8f265679108b2e857db2eccef5558624f4f198153f20ccfaee7c916662c9ffefc2a8ab9faa27bd0e0fe7068033336a097e554", "043229035f1757a2ed69a63d4f041ce708d04f73182b0f792bd2b8ba0292dc4ef61751b30e2a148cd179c06b2513dfde177c0ab46b6b4d5a0c172da39019adbfa1", "047cc5702707175a0574ac93a6028218eaf59620100f30d026fb03a8ae1b673f4ab1efcba56dcd531f03b5d3567a9b3ef39618c139f199194c21a2ab4b057d0216", "041374067e5bde41a6edaa8f6635fc2beabf267514a0182b4c0d310c3f2d25e2d70e7bed4e5922933690d0e530210142ea1fc6da7b329d01bcb7835bb1e6224c48", "04dfcded487b4de10a9b2d33f1f0374f0ea09fe08f63dad07c4b99f4054faa30156d8670332cc87362bd2b6057e96b5af3dc49a9a67fdb0ed0d0aa19d39c3dffa6", "04579bfa77a6366a29e85ec5bbac02d72817c9638b8fc12e5f39b20571f20425f6b44b6d4a5b58e27162f11dd2c88885135edbb42af6b0628f7a98390466c1d5b1", "044efe402dbb86e8732e1ff6d07cd43ca4553151e48490e1a5844f491a72d753bdf934c9441fec5ed7d99c7df2172da4fd25bbc97721efe20da84617672c334a0f", "043eb86aedc3d1eaa5aeecf3c52b6478c638ce969197b32f39570c8061bfa2f88ccdcca958cc3eb2bd29c5e0403e7b771e8b265ef1d8ee6b31e68ad1ddaa8fe9fc"]
        // [56,10,0,99,17,98,34,12,7,77]
        var keyspriv = [
            "9bfee3f3e528e7380fa13ff781bd5141cd22e2bf1806267642f78da6269718e0",
            "ba2e27ef7c74d4cbc3141d71191caf15d513284076b94c2c2b6d180c0839da88",
            "8799196adeb0c2508cc184f90ad8b7ba6ff5a46ce670b4066e707520f32fdabd",
            "9baf1b911d121d250e5e5a793b05d059b3487f8ce408cfb360b0fb917d134bbe",
            "aa07226544abc12b82d764cc075bc185a2a579faeccea8753aec4b05d093ee2f",
            "c3343d9b7df963296b4c1c1656b07f256d5042971aa7e82b0bd933d9ed9e8525",
            "7cd93c2d17178c518359bd9ba336453c119002369603c70f8b968f9b13c10ee3",
            "3ee844a2d628190bf293a3d6ec68af3bee03f80286f7fce38156acbf740e4839",
            "a5ba4620fd302910ab7c9978048c9a8cb999a31f86c0d2fe1897d8783e5b3b95",
            "8217e4d29ce9a85ee8f9ea140c4cda605be75f709bd3eae94274f4823a46efa3"
        ]
        var keyspriv2 = [
            "efa1bbcbf319ee9b4558a652fe74fa7e12948c4a1616e5b1495d38312f87f6ce",
            "11cf41b5cdf08f54a068ae5175a8f2316a146c0fa6c608419af0561d804eac1f",
            "5d0587dedfa753ef3d363d08b7058c9931276a3197b81f188481204c2d04f79a",
            "b096ce3444992f375ab525d89e5519155261e950441782af8d7738e427bf722f",
            "9e074a963e10932c453ce664a51c05f031c1bb4a60f29def69e45597e3240d6d",
            "daf876ef46c9c97f5fc67495ee35b49a8743fedc953e27c70b58f5c73b3fc2d7",
            "6bcf9c6155b1f01e96ff15d2d2a90b669d89b82ab664c6d28a3f3068dff0eb5c",
            "ea4bce3a6482a7d28200aa6af12d27e8b520702c8594bd1995f567eed65b2826",
            "cbf52e5d650bacdff5a256d0ef6fc7f4431323b38d674a4d903ec6438686fad6",
            "9d0c898fec2848c0975038a59fa0714ec78a558590dac0b6da307640df840dbf"
        ]
        var positions = [56, 10, 0, 99, 17, 98, 34, 12, 7, 77]
        for (var i = 0; i < 10; i++) {
            let kp = ec.keyFromPrivate(keyspriv[i], 'hex');
            let pos = positions[i]
            let ring = makeKeyring(keys, kp, pos)
            let signature = sign(payload, ring, kp, pos)
            signature.Ring[i] = ec.keyFromPrivate(keyspriv2[i], 'hex');
            let isValid = verify(signature, keys)
            expect(isValid).to.be.equal(false)
        }
    })
})

describe("Signature verification", () => {
    it("Should verify a signature generated by a member of the public key set", () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("d312fd860f31b0376ac3c033dedb20405846d75aa72855c2eb999decbfef37b2", 'hex');
        var size = 13;
        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);
        const position = 7;
        let keyList = [
            "041e6c02998942e9d145ab750a9385cdba2a6d4ca2532aaf1ebf690cbfbbedd2a7d3f39173ddc836389452d94a496d35c6a46ebf9b8ad614ea04ae825a35035bde", "04ac510d378f342728e05e78c42baf15982d4d2b5f98798f8887f43c7c1b4f36d2223bb67fb7c3ac9bcef5021fe5e5ad529a0bcf127f47a1e2bad8f1eba8489916", "04bd2d44c42c8eec6d7e6a055379010d47a2712f4e1351f5a19e6c9689b970b1c13e89e39af144b945a47faf86edc33a8be5d0d73d8b8533a2344324cfbbeeedd7", "040399e64ab0cea3b66120c9f191c5d868869c237731e426e60cbac2718f4e01c05bdade9ddb10de0e2cce6c6ffefdd465a131a857a117cdc15796d3703f7057ae", "042844ffff648570acc926e459c34103b70a50eda2131b2ccdfb37551dfed4be9c5764b5a4b334f94ba0088656f38727755f2aa36b061c42ad92270b0c0b490a2a", "04db480470493b3409bef886349eccdb6f48f54a2663e0b658741e238f3d5b508916fda0bcb61c9860591e8c1d9872d9b1462e2415479a9e1e28b2660bafb33e1e", "04a74c01f326ed64bd032c5a08af3e27740da649cf7c8c0f0fb6a3a1e1858a7a6053663855f8e29b4943d59f806d5798903276abf94a6231d1140a0fb4ccae87b7", "0465020be4185928ed1055df9d5cc7dd0f8fbdc58ae5842f3f7acddf30010d0080ec2f90cb14bb217100d21039c3d7b8bdb30dbe774d8395adc1d489ab015f3a3f", "04e2d8369c5b4aac8a647b25e39ca1eac993a641e0b507886bf51353b75e4719fa3d411a35c662dc86b3409b6ace044b26a57f4dbc1b88ee0459bb19275a55e2f2", "0421789e362564881937b2a72f74942f84bd45f51b72da5f52773bcd94ac81e8618190e46e8a44354bddd4ca8350bfe1dcf47e4c32c500e9975b4bbb0581aa3c75", "04d8e0fa31b40e5906cdac0d7b9a86d70cf7434adb37a1c58f135cc84e9bee96a0d86a08b3aaba5009fcabb9cd9956cdf2347fe9bec3b199bde57778e9414561c1", "049f63537352808d89cf0c4146df0625e656394b9bf10d7de99fdcca546247df72764720e1b86604fae52f61185a171e00881e4208833bd8e172744d31800b8b1d"
        ]
        let ring = makeKeyring(keyList, keyPair, position);
        let s = sign(payload, ring, keyPair, position);
        let res = verify(s, keyList)
        expect(res).to.be.equal(true)



    })
    it("Should reject a signature generated by an extraneous member", () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("d312fd860f31b0376ac3c033dedb20405846d75aa72855c2eb999decbfef37b2", 'hex');
        var size = 13;
        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);
        const position = 7;
        let keyList = [
            "041e6c02998942e9d145ab750a9385cdba2a6d4ca2532aaf1ebf690cbfbbedd2a7d3f39173ddc836389452d94a496d35c6a46ebf9b8ad614ea04ae825a35035bde", "04ac510d378f342728e05e78c42baf15982d4d2b5f98798f8887f43c7c1b4f36d2223bb67fb7c3ac9bcef5021fe5e5ad529a0bcf127f47a1e2bad8f1eba8489916", "04bd2d44c42c8eec6d7e6a055379010d47a2712f4e1351f5a19e6c9689b970b1c13e89e39af144b945a47faf86edc33a8be5d0d73d8b8533a2344324cfbbeeedd7", "040399e64ab0cea3b66120c9f191c5d868869c237731e426e60cbac2718f4e01c05bdade9ddb10de0e2cce6c6ffefdd465a131a857a117cdc15796d3703f7057ae", "042844ffff648570acc926e459c34103b70a50eda2131b2ccdfb37551dfed4be9c5764b5a4b334f94ba0088656f38727755f2aa36b061c42ad92270b0c0b490a2a", "04db480470493b3409bef886349eccdb6f48f54a2663e0b658741e238f3d5b508916fda0bcb61c9860591e8c1d9872d9b1462e2415479a9e1e28b2660bafb33e1e", "04a74c01f326ed64bd032c5a08af3e27740da649cf7c8c0f0fb6a3a1e1858a7a6053663855f8e29b4943d59f806d5798903276abf94a6231d1140a0fb4ccae87b7", "0465020be4185928ed1055df9d5cc7dd0f8fbdc58ae5842f3f7acddf30010d0080ec2f90cb14bb217100d21039c3d7b8bdb30dbe774d8395adc1d489ab015f3a3f", "04e2d8369c5b4aac8a647b25e39ca1eac993a641e0b507886bf51353b75e4719fa3d411a35c662dc86b3409b6ace044b26a57f4dbc1b88ee0459bb19275a55e2f2", "0421789e362564881937b2a72f74942f84bd45f51b72da5f52773bcd94ac81e8618190e46e8a44354bddd4ca8350bfe1dcf47e4c32c500e9975b4bbb0581aa3c75", "04d8e0fa31b40e5906cdac0d7b9a86d70cf7434adb37a1c58f135cc84e9bee96a0d86a08b3aaba5009fcabb9cd9956cdf2347fe9bec3b199bde57778e9414561c1", "049f63537352808d89cf0c4146df0625e656394b9bf10d7de99fdcca546247df72764720e1b86604fae52f61185a171e00881e4208833bd8e172744d31800b8b1d"
        ]
        let ring = makeKeyring(keyList, keyPair, position);
        let s = sign(payload, ring, keyPair, position);
        let keyList2 = [
            "041e6c02998942e9d145ab750a9385cdba2a6d4cb2532aaf1ebf690cbfbbedd2a7d3f39173ddc836389452d94a496d35c6a46ebf9b8ad614ea04ae825a35035bde", "04ac510d378f342728e05e78c42baf15982d4d2b5f98798f8887f43c7c1b4f36d2223bb67fb7c3ac9bcef5021fe5e5ad529a0bcf127f47a1e2bad8f1eba8489916", "04bd2d44c42c8eec6d7e6a055379010d47a2712f4e1351f5a19e6c9689b970b1c13e89e39af144b945a47faf86edc33a8be5d0d73d8b8533a2344324cfbbeeedd7", "040399e64ab0cea3b66120c9f191c5d868869c237731e426e60cbac2718f4e01c05bdade9ddb10de0e2cce6c6ffefdd465a131a857a117cdc15796d3703f7057ae", "042844ffff648570acc926e459c34103b70a50eda2131b2ccdfb37551dfed4be9c5764b5a4b334f94ba0088656f38727755f2aa36b061c42ad92270b0c0b490a2a", "04db480470493b3409bef886349eccdb6f48f54a2663e0b658741e238f3d5b508916fda0bcb61c9860591e8c1d9872d9b1462e2415479a9e1e28b2660bafb33e1e", "04a74c01f326ed64bd032c5a08af3e27740da649cf7c8c0f0fb6a3a1e1858a7a6053663855f8e29b4943d59f806d5798903276abf94a6231d1140a0fb4ccae87b7", "0465020be4185928ed1055df9d5cc7dd0f8fbdc58ae5842f3f7acddf30010d0080ec2f90cb14bb217100d21039c3d7b8bdb30dbe774d8395adc1d489ab015f3a3f", "04e2d8369c5b4aac8a647b25e39ca1eac993a641e0b507886bf51353b75e4719fa3d411a35c662dc86b3409b6ace044b26a57f4dbc1b88ee0459bb19275a55e2f2", "0421789e362564881937b2a72f74942f84bd45f51b72da5f52773bcd94ac81e8618190e46e8a44354bddd4ca8350bfe1dcf47e4c32c500e9975b4bbb0581aa3c75", "04d8e0fa31b40e5906cdac0d7b9a86d70cf7434adb37a1c58f135cc84e9bee96a0d86a08b3aaba5009fcabb9cd9956cdf2347fe9bec3b199bde57778e9414561c1", "049f63537352808d89cf0c4146df0625e656394b9bf10d7de99fdcca546247df72764720e1b86604fae52f61185a171e00881e4208833bd8e172744d31800b8b1d"
        ]
        let res = verify(s, keyList2)
        expect(res).to.be.equal(false)
    })
    it("Should reject a tampered signature", () => {
        it("Should reject a signature generated by an extraneous member", () => {
            var ec = new EC('secp256k1');
            let keyPair = ec.keyFromPrivate("d312fd860f31b0376ac3c033dedb20405846d75aa72855c2eb999decbfef37b2", 'hex');
            var size = 13;
            const hash = new SHA3(256);
            hash.update('helloworld');
            let message_hash = hash.digest('hex');
            let input = Buffer.from(message_hash, 'hex');
            const payload = Uint8Array.from(input);
            const position = 7;
            let keyList = [
                "041e6c02998942e9d145ab750a9385cdba2a6d4ca2532aaf1ebf690cbfbbedd2a7d3f39173ddc836389452d94a496d35c6a46ebf9b8ad614ea04ae825a35035bde", "04ac510d378f342728e05e78c42baf15982d4d2b5f98798f8887f43c7c1b4f36d2223bb67fb7c3ac9bcef5021fe5e5ad529a0bcf127f47a1e2bad8f1eba8489916", "04bd2d44c42c8eec6d7e6a055379010d47a2712f4e1351f5a19e6c9689b970b1c13e89e39af144b945a47faf86edc33a8be5d0d73d8b8533a2344324cfbbeeedd7", "040399e64ab0cea3b66120c9f191c5d868869c237731e426e60cbac2718f4e01c05bdade9ddb10de0e2cce6c6ffefdd465a131a857a117cdc15796d3703f7057ae", "042844ffff648570acc926e459c34103b70a50eda2131b2ccdfb37551dfed4be9c5764b5a4b334f94ba0088656f38727755f2aa36b061c42ad92270b0c0b490a2a", "04db480470493b3409bef886349eccdb6f48f54a2663e0b658741e238f3d5b508916fda0bcb61c9860591e8c1d9872d9b1462e2415479a9e1e28b2660bafb33e1e", "04a74c01f326ed64bd032c5a08af3e27740da649cf7c8c0f0fb6a3a1e1858a7a6053663855f8e29b4943d59f806d5798903276abf94a6231d1140a0fb4ccae87b7", "0465020be4185928ed1055df9d5cc7dd0f8fbdc58ae5842f3f7acddf30010d0080ec2f90cb14bb217100d21039c3d7b8bdb30dbe774d8395adc1d489ab015f3a3f", "04e2d8369c5b4aac8a647b25e39ca1eac993a641e0b507886bf51353b75e4719fa3d411a35c662dc86b3409b6ace044b26a57f4dbc1b88ee0459bb19275a55e2f2", "0421789e362564881937b2a72f74942f84bd45f51b72da5f52773bcd94ac81e8618190e46e8a44354bddd4ca8350bfe1dcf47e4c32c500e9975b4bbb0581aa3c75", "04d8e0fa31b40e5906cdac0d7b9a86d70cf7434adb37a1c58f135cc84e9bee96a0d86a08b3aaba5009fcabb9cd9956cdf2347fe9bec3b199bde57778e9414561c1", "049f63537352808d89cf0c4146df0625e656394b9bf10d7de99fdcca546247df72764720e1b86604fae52f61185a171e00881e4208833bd8e172744d31800b8b1d"
            ]
            let ring = makeKeyring(keyList, keyPair, position);
            let s = sign(payload, ring, keyPair, position);
            s.C.divn(2)
            let res = verify(s, keyList)
            expect(res).to.be.equal(false)
        })
    })
})

describe("Signature linking", () => {
    it("Should link two signatures coming from the same signer", () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        var size = 10;

        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);
        const position = Math.floor(Math.random() * size);

        let ring = test_util.GenNewKeyRing(size, keyPair, position);
        let s = sign(payload, ring, keyPair, position);
        let s2 = sign(payload, ring, keyPair, position);
        let res = link(s, s2)
        expect(res).to.be.equal(true)
    })
    it("Should link two signatures coming from the same signer on a different message", () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        var size = 10;

        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);
        const position = Math.floor(Math.random() * size);

        const hash2 = new SHA3(256);
        hash2.update('helloworld2');
        let message_hash2 = hash2.digest('hex');
        let input2 = Buffer.from(message_hash2, 'hex');
        const payload2 = Uint8Array.from(input2);

        let ring = test_util.GenNewKeyRing(size, keyPair, position);
        let s = sign(payload, ring, keyPair, position);
        let s2 = sign(payload2, ring, keyPair, position);
        let res = link(s, s2)
        expect(res).to.be.equal(true)
    })
    it("Should not link two signatures coming from different accounts", () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        let keyPair2 = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3804", 'hex');
        var size = 10;

        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);
        const position = Math.floor(Math.random() * size);

        let ring = test_util.GenNewKeyRing(size, keyPair, position);
        let ring2 = test_util.GenNewKeyRing(size, keyPair2, position);
        let s = sign(payload, ring, keyPair, position);
        let s2 = sign(payload, ring2, keyPair2, position);
        let res = link(s, s2)
        expect(res).to.be.equal(false)
    })
    it("Should not link two signatures coming from different accounts on different messages", () => {
        var ec = new EC('secp256k1');
        let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
        let keyPair2 = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3804", 'hex');
        var size = 10;

        const hash = new SHA3(256);
        hash.update('helloworld');
        let message_hash = hash.digest('hex');
        let input = Buffer.from(message_hash, 'hex');
        const payload = Uint8Array.from(input);
        const position = Math.floor(Math.random() * size);

        const hash2 = new SHA3(256);
        hash2.update('helloworld2');
        let message_hash2 = hash2.digest('hex');
        let input2 = Buffer.from(message_hash2, 'hex');
        const payload2 = Uint8Array.from(input2);

        let ring = test_util.GenNewKeyRing(size, keyPair, position);
        let ring2 = test_util.GenNewKeyRing(size, keyPair2, position);
        let s = sign(payload, ring, keyPair, position);
        let s2 = sign(payload2, ring2, keyPair2, position);
        let res = link(s, s2)
        expect(res).to.be.equal(false)
    })
})

describe("Intercompatibility", () => {
    it("Should accept key pairs generated from Ethereum libraries", () => {
        // LEGIT
        let privateKeyInCensus
        const censusPublicKeys = []  // pub keys
        for (let i = 0; i < 10; i++) {
            const wallet = Wallet.createRandom()
            if (i == 5) privateKeyInCensus = wallet.privateKey // our key

            wallet.getAddress()
            censusPublicKeys.push(wallet["signingKey"].publicKey)
        }

        // FAKE
        const w = Wallet.createRandom()
        const extraneousId = {
            privateKey: w.privateKey,
            publicKey: w["signingKey"].publicKey
        }

        // VALID SIGNATURES
        const signature1 = mainSign("hello world", privateKeyInCensus, censusPublicKeys)
        const isValid1 = verify(signature1, censusPublicKeys)
        expect(signature1).to.be.ok
        expect(isValid1).to.be.true

        const signature2 = mainSign("hi there", privateKeyInCensus, censusPublicKeys)
        const isValid2 = verify(signature2, censusPublicKeys)
        expect(signature2).to.be.ok
        expect(isValid2).to.be.true

        const signaturesLinked = link(signature1, signature2)
        expect(signaturesLinked).to.be.true

        // INVALID  SIGNATURES
        try {
            // A random private key not in the ring
            const extraneousPrivateKey = extraneousId.privateKey

            const fakeSignature = mainSign("hello world", extraneousPrivateKey, censusPublicKeys)
            throw new Error("❌ Should have thrown an error")
        }
        catch (err) {
            expect(err.message).to.eq("The given key pair does not match with any public key in the array")
        }

    })
})

describe("Internal computations", () => {

    describe("Random key ring generation", () => {
        it("should generate a random keyring with our own key at a given index", () => {
            let ec = new EC('secp256k1')

            const size = 12
            const randomIndexes = []
            for (let i = 0; i < size; i++) {
                randomIndexes.push(Math.floor(Math.random() * size))
            }

            randomIndexes.forEach(idx => {
                const myKeyPair = ec.genKeyPair()
                const keyRing = test_util.GenNewKeyRing(size, myKeyPair, idx)

                expect(keyRing[idx].getPublic().getX().toString(16)).to.eq(myKeyPair.getPublic().getX().toString(16))
                expect(keyRing[idx].getPublic().getY().toString(16)).to.eq(myKeyPair.getPublic().getY().toString(16))
            })
        })
        it("should never put the given key anywhere else than the given index", () => {
            let ec = new EC('secp256k1')

            const size = 12
            const randomIndexes = []
            for (let i = 0; i < 6; i++) {
                randomIndexes.push(Math.floor(Math.random() * size))
            }

            randomIndexes.forEach(idx => {
                const myKeyPair = ec.genKeyPair()
                const keyRing = test_util.GenNewKeyRing(size, myKeyPair, idx)

                keyRing.filter((kr, i) => i != idx).forEach(keyPair => {
                    expect(keyPair.getPublic().getX().toString(16)).to.not.eq(myKeyPair.getPublic().getX().toString(16))
                    expect(keyPair.getPublic().getY().toString(16)).to.not.eq(myKeyPair.getPublic().getY().toString(16))
                })
            })
        })
        it("shoud not allow to specify signer key index beyond the ring size", () => {
            let ec = new EC('secp256k1')
            const myKeyPair = ec.genKeyPair()
            const size = 10
            test_util.GenNewKeyRing(size, myKeyPair, 4)
            try {
                test_util.GenNewKeyRing(size, myKeyPair, 15)
                throw new Error("it should throw an error but it did not")
            } catch (err) {
                expect(err.message).contain("Position out of bounds")
            }
        })
        it("should not allow to specify a ring with length less than 2", () => {
            let ec = new EC('secp256k1')
            const myKeyPair = ec.genKeyPair()
            try {
                test_util.GenNewKeyRing(1, myKeyPair, 0)
                throw new Error("can generate the ring with length less than 2")
            } catch (err) {
                expect(err.message).contain("The ring size is too small")
            }
        })
    })

    describe("Key ring generation given N public keys", () => {
        /*
        it("should allow to create a key ring with imported public keys", () => {
            let ec = new EC('secp256k1')
            const myKeyPair = ec.genKeyPair()
            let kr = makeKeyring(pubKeyList, myKeyPair, 3)
            expect(kr).to.be.ok
        })
        it("should never put the given key anywhere else than the given index", () => {
            let ec = new EC('secp256k1')
            const myKeyPair = ec.genKeyPair()
            const myKeyPairPriv = myKeyPair.getPrivate().toString()
            let kr = makeKeyring(pubKeyList, myKeyPair, 3)
            kr.forEach((k, idx) => {
                if (idx != 3) {
                    try {
                        k[idx].getPrivate()
                    } catch (err) {
                        expect(err.message).contain("Cannot read property 'getPrivate'")
                    }
                }
            })
            expect(kr[3].getPrivate().toString()).to.be.equal(myKeyPairPriv)
        })
        it("shoud not allow to specify signer key index beyond the ring size", () => {
            let ec = new EC('secp256k1')
            const myKeyPair = ec.genKeyPair()
            try {
                makeKeyring(pubKeyList, myKeyPair, 6)
                throw new Error('it should throw an error but it did not')
            } catch (e) {
                expect(e.message).to.contain('Position out of bounds')
            }
        })
        */
        it("should not allow to specify a ring with length less than 2", () => {
            const pubKeyList = []
            let ec = new EC('secp256k1')
            const myKeyPair = ec.genKeyPair()
            try {
                makeKeyring(pubKeyList, myKeyPair, 0)
                throw new Error('it should throw an error but it did not')
            } catch (e) {
                expect(e.message).to.contain('The ring size is too small')
            }
        })
    })

    describe('hashPoint, sha3(P) * G', () => {
        it('should get the correct public key given a keyPair', () => {
            var ec = new EC('secp256k1');
            let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
            let x = keyPair.getPublic().getX().toString(10)
            let y = keyPair.getPublic().getY().toString(10)
            expect(x).to.be.equal("61624232139996154006984748081984944058027168161890417162476666845838569842209")
            expect(y).to.be.equal("90637217951542034542483916518656422557977730750643746603453994398523581456489")
        })
        it('should multiply the curve generator and the hash of P correctly', () => {
            var ec = new EC('secp256k1');
            let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
            let res = hashPoint(keyPair)
            expect(res.getX().toString(10)).to.be.equal("107542857799300540860467213237685733629532601766257792261885338079725119149910")
            expect(res.getY().toString(10)).to.be.equal("71057034266221250709557434678667012805631488218211020174703753646050447590851")
        })
    })
    describe('Computes a key image using getKeyImage', () => {
        it('should get a key image given a keyPair', () => {
            var ec = new EC('secp256k1');
            let keyPair = ec.keyFromPrivate("358be44145ad16a1add8622786bef07e0b00391e072855a5667eb3c78b9d3803", 'hex');
            let res = getKeyImage(keyPair)
            expect(res.getX().toString(10)).to.be.equal("11074425971893196108669913839764794868434406111970427592825829042055020800084")
            expect(res.getY().toString(10)).to.be.equal("69625022024291189122044636994488630043661763242714914627004347424029720229548")
        })
    })

})
