var randomBytes = require('randombytes');
import { ec as EC } from 'elliptic';
import { SHA3 as SHA3 } from 'sha3';
import { Buffer } from 'buffer';
import BN from 'bn.js';
import { KeyPair, RingSign, KeyRing } from '../src/lib/ring';
import { getKeyImage, hashPoint, toHexString } from '../src/lib/utils';

/**
 * create ring signature from list of public keys given inputs 
 * @param msg message to sign
 * @param ring ring public keys array
 * @param keyPair ring signer keyPair
 * @param signerPosition ring size
 */
export function SignTEST(msg: Uint8Array, ring: KeyRing, keyPair: KeyPair, signerPosition: number) {
    let ringSize = ring.length;

    if (ringSize < 2) {
        throw new Error("size less than two does not make sense");
    }
    else if (signerPosition >= ringSize || signerPosition < 0) {
        throw new Error("secret index out of range of ring size");
    }

    let pubKey = keyPair.getPublic();


    let sig = new RingSign(ringSize, msg, ring);

    // check that key at index signerPosition is indeed the signer's
    const ringSignerX = ring[signerPosition].getPublic().getX().toString(10)
    const ringSignerY = ring[signerPosition].getPublic().getY().toString(10)
    const givenPubKeyX = pubKey.getX().toString(10)
    const givenPubKeyY = pubKey.getY().toString(10)
    if (ringSignerX != givenPubKeyX || ringSignerY != givenPubKeyY) {
        throw new Error("secret index in ring is not signer");
    }

    // generate key image
    let image = getKeyImage(keyPair);
    sig.I = image;

    // start at c[1]
    // pick random scalar u (glue value), calculate c[1] = H(m, u*G) where H is a hash function and G is the base point of the curve
    let C: BN[] = [];
    let S: BN[] = [];

    // pick random scalar u
    //let u = getRandomBigNumber();
    let u = new BN("30303");  // FOR TESTING

    // start at secret index signerPosition
    // compute L_s = u*G
    let L_s = keyPair.ec.g.mul(u);

    // compute R_s = u*H_p(P[signerPosition])
    let H_p = hashPoint(keyPair)
    let R_s = H_p.mul(u);

    let l = Buffer.from(L_s.x.toArray('big').concat(L_s.y.toArray('big')))
    let r = Buffer.from(R_s.x.toArray('big').concat(R_s.y.toArray('big')))

    // concatenate m and u*G and calculate c[signerPosition+1] = H(m, L_s, R_s)
    let hash = new SHA3(256);
    hash.update(toHexString([...[...msg], ...[...[...l], ...[...r]]]), 'hex');
    let C_i = Uint8Array.from(Buffer.from(hash.digest('hex'), 'hex'));
    let idx = (signerPosition + 1) % ringSize;
    C[idx] = new BN(C_i)


    // start loop at signerPosition+1
    for (let i = 1; i < ringSize; i++) {
        idx = (signerPosition + i) % ringSize;
        let s_i = new BN("30303")       // FOR TESTING
        //let s_i = getRandomBigNumber();
        S[idx] = s_i;

        // calculate L_i = s_i*G + c_i*P_i
        let p = ring[idx].getPublic().mul(C[idx]); // c_i * P_i
        let s = keyPair.ec.curve.g.mul(s_i);       // sx, sy = s[n-1]*G
        let lxy = p.add(s);

        // calculate R_i = s_i*H_p(P_i) + c_i*I
        p = image.mul(C[idx])
        H_p = hashPoint(ring[idx])
        s = H_p.mul(s_i)
        let r = p.add(s);

        // calculate c[i+1] = H(m, L_i, R_i)
        hash = new SHA3(256);
        l = Buffer.from(lxy.x.toArray('big').concat(lxy.y.toArray('big')))
        r = Buffer.from(r.x.toArray('big').concat(r.y.toArray('big')))
        hash.update(toHexString([...[...msg], ...[...[...l], ...[...r]]]), 'hex');
        C_i = Uint8Array.from(Buffer.from(hash.digest('hex'), 'hex'));

        if (i == ringSize - 1) {
            C[signerPosition] = new BN(C_i)
        }
        else {
            C[(idx + 1) % ringSize] = new BN(C_i)
        }
    }

    // close ring by finding S[signerPosition] = ( u - c[signerPosition]*k[signerPosition] ) mod P where k[signerPosition] is the private key and P is the order of the curve
    let cs_mul_ks = C[signerPosition].mul(keyPair.getPrivate() as BN);
    let u_sub_csmulks = cs_mul_ks.neg().add(u);
    let mod = u_sub_csmulks.umod(new BN(keyPair.ec.curve.n.toString(10)));
    S[signerPosition] = mod;

    // check that u*G = S[signerPosition]*G + c[signerPosition]*P[signerPosition]
    let check_u = keyPair.ec.curve.g.mul(u);
    let check_p = ring[signerPosition].getPublic().mul(C[signerPosition]);
    let check_s = keyPair.ec.curve.g.mul(S[signerPosition]);
    let check_r = check_s.add(check_p);

    // check that u*H_p(P[signerPosition]) = S[signerPosition]*H_p(P[signerPosition]) + C[signerPosition]*I
    let check_p2 = image.mul(C[signerPosition]);         // px, py = C[signerPosition]*I
    let check_h2 = hashPoint(ring[signerPosition]);
    let check_t2 = check_h2.mul(u);
    let check_s2 = check_h2.mul(S[signerPosition]);      // sx, sy = S[signerPosition]*H_p(P[signerPosition])
    let check_r2 = check_s2.add(check_p2);

    // check that H(m, L[signerPosition], R[signerPosition]) == C[signerPosition+1]
    l = Buffer.from(check_r.x.toArray('big').concat(check_r.y.toArray('big')))
    r = Buffer.from(check_r2.x.toArray('big').concat(check_r2.y.toArray('big')))
    hash = new SHA3(256);
    hash.update(toHexString([...[...msg], ...[...[...l], ...[...r]]]), 'hex');
    C_i = Uint8Array.from(Buffer.from(hash.digest('hex'), 'hex'));

    if ((check_u.x.toString(10) != check_r.x.toString(10)) ||
        (check_u.y.toString(10) != check_r.y.toString(10)) ||
        (check_t2.x.toString(10) != check_r2.x.toString(10)) ||
        (check_t2.y.toString(10) != check_r2.y.toString(10))) {
        throw new Error("Error closing ring");
    }

    sig.S = S;
    sig.C = C[0]

    return sig
}


// creates a ring with size specified by `size` and places the public key corresponding to `privkey` in index s of the ring
export function GenNewKeyRingTEST(size: number, keyPair: EC.KeyPair, s: number): Array<EC.KeyPair> {
    let ring = new Array<EC.KeyPair>(size);
    let ec = new EC('secp256k1');
    if (s > ring.length) {
        throw new Error("index s out of bounds");
    }
    keyPair.getPublic()
    ring[s] = keyPair

    let priv_arr = []
    priv_arr[0] = ec.keyFromPrivate("a86d4ac11bb94d0cf7d895e0591d9e5b8a9ba38c494963b43d41a62f317de875", 'hex')
    priv_arr[1] = ec.keyFromPrivate("82e6d386cfc519ad2a4d9f2d944938d5983fdb97214afb522b08357112e3bf96", 'hex')
    priv_arr[2] = ec.keyFromPrivate("b64fa025f13c6896eec8c7ef8867fe02846855ae1a2fc202c6c9d1968cce2c87", 'hex')
    priv_arr[3] = ec.keyFromPrivate("e912d6b4e93d2155401e01792e2ceb1a2b12296497de4480b421b48e95ec8994", 'hex')
    priv_arr[4] = ec.keyFromPrivate("2292f57c8a43dde8a4332de6f8b98c68a0987299a8ff0ea97f2b356c27e5acff", 'hex')
    priv_arr[5] = ec.keyFromPrivate("a452dc6deb4293e0d8645f89db41a022aa2929414d179371be50c1088df58d97", 'hex')
    priv_arr[6] = ec.keyFromPrivate("ec4751a0a093421d940fb75bd5e84224995299d860734bfea92aa5a33df2c8c2", 'hex')
    priv_arr[7] = ec.keyFromPrivate("a86d4ac11bb94d0cf7d895e0591d9e5b8a9ba38c494963b43d41a62f317de875", 'hex')
    priv_arr[8] = ec.keyFromPrivate("3d3ccb5f60c6fa280d7324e8eaa8db954b266b132298a3485db697bfab8feb05", 'hex')
    priv_arr[9] = ec.keyFromPrivate("c034f0e911b76f86646ef4194a47edb97203beb26ac57ed2effe5b5d4c7352a7", 'hex')
    priv_arr[10] = ec.keyFromPrivate("f4ffe467c62e6c499c356f156439130c9a1be34bbe93feabfaea2894cafa16e1", 'hex')
    priv_arr[11] = ec.keyFromPrivate("48e9bdcc102a67089ea5334453908c45ec5b7481ec13cf17839d518c4042bcee", 'hex')

    for (var i = 1; i < size; i++) {
        var idx = (i + s) % size;
        priv_arr[i - 1].getPublic()
        //var priv = ec.genKeyPair();    ///// THIS IS CORRECT BUT FOR NOW PRIVK HARDCODED
        var priv = priv_arr[i - 1]
        ring[idx] = priv
    }
    return ring;
}

/**
 * Creates a ring with size specified by size and places the public key 
 * corresponding to privkey in index position of the ring
 * @param size ring size
 * @param keyPair rign signer keypair
 * @param position position where the real signing key should be placed
 */
export function GenNewKeyRing(size: number, keyPair: KeyPair, position: number): KeyRing {
    let ring = [];

    if (size < 2) {
        throw new Error("The ring size is too small")
    }

    if (position > size) {
        throw new Error("Position out of bounds")
    }

    let ec = new EC('secp256k1');

    for (let i = 0; i < size; i++) {
        if (i != position) {
            ring[i] = ec.genKeyPair()
        }
        else {
            ring[i] = keyPair
        }
    }

    return ring;
}

/**
 * Verifies a signature
 * @param sig the signature
 */
export function VerifyTEST(sig): boolean {
    let ring = sig.Ring;
    let ringSize = sig.Size;
    let S = sig.S;
    let C: BN[] = [];
    C[0] = sig.C
    let image = sig.I;

    // calculate c[i+1] = H(m, s[i]*G + c[i]*P[i])
    // and c[0] = H)(m, s[n-1]*G + c[n-1]*P[n-1]) where n is the ring size
    for (let i = 0; i < ringSize; i++) {
        // calculate L_i = s_i*G + c_i*P_i
        let p = ring[i].getPublic().mul(C[i]);
        let s = ring[i].ec.curve.g.mul(S[i]);
        let l = s.add(p);

        // calculate R_i = s_i*H_p(P_i) + c_i*I
        p = image.mul(C[i]);
        let h = hashPoint(ring[i]);
        s = h.mul(S[i]);
        let r = s.add(p);

        // calculate c[i+1] = H(m, L_i, R_i)
        let l_2 = Buffer.from(l.x.toArray('big').concat(l.y.toArray('big')));
        let r_2 = Buffer.from(r.x.toArray('big').concat(r.y.toArray('big')));

        let pre_c_i = [...sig.M, ...[...[...l_2], ...[...r_2]]];
        let hash = new SHA3(256);
        hash.update(toHexString(pre_c_i), 'hex');
        let c_i = Uint8Array.from(Buffer.from(hash.digest('hex'), 'hex'));

        if (i == ringSize - 1) {
            C[0] = new BN(c_i);
        }
        else {
            C[i + 1] = new BN(c_i);
        }
    }

    return sig.C.eq(C[0])
}